//
//  DeviceIdProvider.h
//  deviceId
//
//  Created by Mattias Muhonen on 25/04/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

// CalendarManager.h
#import <React/RCTBridgeModule.h>
@interface DeviceIdProvider : NSObject <RCTBridgeModule>
@end
