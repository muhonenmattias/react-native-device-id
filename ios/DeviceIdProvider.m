//
//  DeviceIdProvider.m
//  is the actual implemantation that "hooks" up to the bridge defined in the header
//
//  Created by Mattias Muhonen on 25/04/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <React/RCTLog.h>
#import "DeviceIdProvider.h"
@implementation DeviceIdProvider

RCT_EXPORT_MODULE();

RCT_REMAP_METHOD(getId, // function name
                 resolver:(RCTPromiseResolveBlock)resolve // resolver for async
                 rejecter:(RCTPromiseRejectBlock)reject)  // rejecter for sync
{
    // TODO: get the actual id somehow
    resolve(@"TheAmazingHulks_iPhone");
    // reject(@"no_events", @"There were no events", error);
}

@end
