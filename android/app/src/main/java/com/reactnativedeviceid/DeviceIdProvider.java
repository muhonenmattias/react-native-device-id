package com.reactnativedeviceid;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactBridge;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by mattiasmuhonen on 25/04/2017.
 */

public class DeviceIdProvider extends ReactContextBaseJavaModule {



    public DeviceIdProvider(ReactApplicationContext context){
        super(context);
    }

    @Override
    public String getName(){
        return "DeviceIdProvider";
    }

    @ReactMethod
    public void getId(Promise promise){ // if the last param is promise then it's async
        // TODO: get the actual id
        promise.resolve("WonderWomans_android");
    }

}
