import React from 'react';
import { StyleSheet, Text, View, Button, NativeModules } from 'react-native';
const DeviceIdProvider = NativeModules.DeviceIdProvider;

export default class App extends React.Component {

  state = {
    id: ''
  }

  async getId(){
    const id = await DeviceIdProvider.getId();
    this.setState({
      id
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Push button, get id.</Text>
        <Button onPress={() => this.getId()} title="Push" />
        <Text>Your id: {this.state.id}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
